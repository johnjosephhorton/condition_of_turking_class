import csv, datetime, time
data = csv.reader(open("anonymous.csv",'r'),delimiter=",")
header = data.next()
class Observation(object):
    def __init__(self,header,data): 
        self.attr = dict(zip(header,data))
subjects = []
[subjects.append (Observation(header,line)) for line in data] 

def clean_number(str): 
    str = str.replace("%","")
    str = str.replace("percent","")
    str = str.replace("I would say","")
    try:
        return float(str)
    except ValueError:
        return None

def posix(str):
    return int(time.mktime(time.strptime(str,'%a %b %d %H:%M:%S GMT %Y')))

out = csv.writer(open("exp2data.csv",'w'),delimiter=",")
new_header = ['start','end','country','real','amt']
out.writerow(new_header)
for subject in subjects: 
    subject.real = clean_number(subject.attr['Answer.real'])
    subject.amt = clean_number(subject.attr['Answer.amt'])
    subject.country = subject.attr['Answer.country']
    subject.start = posix(subject.attr['AcceptTime'])
    subject.end = posix(subject.attr['SubmitTime'])
    out.writerow([subject.__dict__[var] for var in new_header] )

    
